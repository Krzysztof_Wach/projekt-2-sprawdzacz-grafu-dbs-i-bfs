from math import ceil, floor
from random import choice, random
import pandas as pd
import matplotlib.pyplot as plt


class Grafomat:
    def __init__(self, plik = 'graf.txt'):
        self.iloscWGrafie = {}
        self.czyPobranoIlosci = False
        self.slownikPunktow = {}
        self.plik = open(plik, 'r')
        self.pobierzDane()

    def pobierzDane(self):
        for linijka in self.plik:
            listaZLinijki = [int(i) for i in linijka.split()]
            if len(listaZLinijki) == 2 and self.czyPobranoIlosci == False:
                self.pobierzIlosciWGrafie(listaZLinijki)
            else:
                self.pobierzPolaczenia(listaZLinijki)

    def pobierzIlosciWGrafie(self, listaZLinijki):
        self.iloscWGrafie['wierzcholki'] = listaZLinijki[0]
        self.iloscWGrafie['polaczenia'] = listaZLinijki[1]
        self.czyPobranoIlosci = True

    def pobierzPolaczenia(self, listaZLinijki):
        for i in range(len(listaZLinijki)):
            if i % 2 == 0:
                if listaZLinijki[i] not in self.slownikPunktow:
                    self.slownikPunktow[listaZLinijki[i]] = Wierzcholek()
                self.slownikPunktow[listaZLinijki[i]].dodajPolaczenie(listaZLinijki[i + 1])

    def trybA(self, rodzaj):
        self.aktualnyKrok = 0
        self.odwiedzone = []
        self.kolejka = [1]
        self.kolor1 = 'zielony'
        self.kolor2 = 'czerwony'
        self.bladDwudzielnosci = '<--- brak dwudzielnosci'
        self.slownikPunktow[1].kolor = self.kolor1

        if rodzaj == 'BFS':
            print('////////////BFS_dwudzielnosc///////////////')
            for i in range(self.iloscWGrafie['wierzcholki']):
                self.bFS()
            print('PODSUMOWANIE')
            for i in self.slownikPunktow:
                licznik = 0
                if self.slownikPunktow[i].znacznikBleduDwudzielnosci != '':
                    print('graf nie jest dwudzielny')
                    break
                else:
                    if licznik == self.iloscWGrafie['wierzcholki']:
                        print('graf jest dwudzielny')
            for i in self.slownikPunktow:
                print('wierzcholek', i, 'ma kolor', self.slownikPunktow[i].kolor,
                      self.slownikPunktow[i].znacznikBleduDwudzielnosci)
        elif rodzaj == 'DFS':
            print('////////////DFS_dwudzielnosc///////////////')
            for i in range(self.iloscWGrafie['wierzcholki']):
                self.dFS()
            print('PODSUMOWANIE')
            licznik = 0
            for i in self.slownikPunktow:
                if self.slownikPunktow[i].znacznikBleduDwudzielnosci != '':
                    print('graf nie jest dwudzielny')
                    break
                else:
                    licznik += 1
                    if licznik == self.iloscWGrafie['wierzcholki']:
                        print('graf jest dwudzielny')
            for i in self.slownikPunktow:
                print('wierzcholek', i, 'ma kolor', self.slownikPunktow[i].kolor,
                      self.slownikPunktow[i].znacznikBleduDwudzielnosci)

    def bFS(self):
        # krok
        self.aktualnyKrok += 1
        print('krok: ', self.aktualnyKrok)
        # odwiedzone
        self.odwiedzone.append(self.kolejka[0])
        # w kolejce
        self.kolejka.remove(self.kolejka[0])
        aktualnyWierzcholek = self.slownikPunktow[self.odwiedzone[-1]]
        for punkt in range(len(aktualnyWierzcholek.polaczenia)):
            if aktualnyWierzcholek.polaczenia[punkt] not in self.kolejka and aktualnyWierzcholek.polaczenia[
                punkt] not in self.odwiedzone:
                self.kolejka.append(aktualnyWierzcholek.polaczenia[punkt])
        # kolor
        for item in aktualnyWierzcholek.polaczenia:
            if self.slownikPunktow[item].kolor != '':
                if self.slownikPunktow[item].kolor == aktualnyWierzcholek.kolor:
                    self.slownikPunktow[item].znacznikBleduDwudzielnosci = self.bladDwudzielnosci
                    print('ustawiam kolor wierzcholka', item, 'na', self.slownikPunktow[item].kolor,
                          self.slownikPunktow[item].znacznikBleduDwudzielnosci)
            else:
                if aktualnyWierzcholek.kolor == self.kolor1:
                    self.slownikPunktow[item].kolor = self.kolor2
                elif aktualnyWierzcholek.kolor == self.kolor2:
                    self.slownikPunktow[item].kolor = self.kolor1
                print('ustawiam kolor wierzcholka', item, 'na', self.slownikPunktow[item].kolor,
                      self.slownikPunktow[item].znacznikBleduDwudzielnosci)

        print(self.odwiedzone, 'na stosie', self.kolejka, '\n')

    def dFS(self):
        # krok
        self.aktualnyKrok += 1
        print('krok: ', self.aktualnyKrok)
        # odwiedzone
        self.odwiedzone.append(self.kolejka[-1])
        # w kolejce
        doStosu = []
        self.kolejka.remove(self.kolejka[-1])
        aktualnyWierzcholek = self.slownikPunktow[self.odwiedzone[-1]]
        for punkt in range(len(aktualnyWierzcholek.polaczenia)):
            if aktualnyWierzcholek.polaczenia[punkt] not in self.kolejka and aktualnyWierzcholek.polaczenia[
                punkt] not in self.odwiedzone:
                doStosu.append(aktualnyWierzcholek.polaczenia[punkt])
        doStosu.sort(reverse=True)
        self.kolejka = self.kolejka + doStosu
        # kolor
        for item in aktualnyWierzcholek.polaczenia:
            if self.slownikPunktow[item].kolor != '':
                if self.slownikPunktow[item].kolor == aktualnyWierzcholek.kolor:
                    self.slownikPunktow[item].znacznikBleduDwudzielnosci = self.bladDwudzielnosci
                    print('ustawiam kolor wierzcholka', item, 'na', self.slownikPunktow[item].kolor,
                          self.slownikPunktow[item].znacznikBleduDwudzielnosci)
            else:
                if aktualnyWierzcholek.kolor == self.kolor1:
                    self.slownikPunktow[item].kolor = self.kolor2
                elif aktualnyWierzcholek.kolor == self.kolor2:
                    self.slownikPunktow[item].kolor = self.kolor1
                print('ustawiam kolor wierzcholka', item, 'na', self.slownikPunktow[item].kolor,
                      self.slownikPunktow[item].znacznikBleduDwudzielnosci)

        print(self.odwiedzone, 'w kolejce', self.kolejka, '\n')

    def trybB(self):
        iloscWierzcholkow = 50
        self.minGestosc = self.obliczMinGestosc(iloscWierzcholkow)
        self.maxGestosc = self.obliczMaxGestosc(iloscWierzcholkow)
        punktStatystyczny = floor((self.maxGestosc - self.minGestosc) / 9)
        self.listaIlosciPolaczen = []
        self.listaIlosciDwudzielnosci = []

        # losowy graf
        c = 0
        for i in range(0, (self.maxGestosc - self.minGestosc), punktStatystyczny):
            iloscNowychPolaczen = i
            for j in range(0, 100):
                self.slownikTrybuB = {}
                for item in range(1, iloscWierzcholkow + 1):
                    self.slownikTrybuB[item] = Wierzcholek()
                self.polaczNajmniejszy()

                self.listaIlosciPolaczen.append(iloscNowychPolaczen + self.minGestosc)
                if self.polaczLosowo(iloscWierzcholkow, iloscNowychPolaczen) == 'tak':
                    self.listaIlosciDwudzielnosci.append('tak')
                else:
                    self.listaIlosciDwudzielnosci.append('nie')
        self.generujWynikTrybuB()

    def obliczMinGestosc(self, iloscWierzcholkow):
        min = iloscWierzcholkow - 1
        return min

    def obliczMaxGestosc(self, iloscWierzcholkow):
        return floor((iloscWierzcholkow * (iloscWierzcholkow - 1)) / 2)

    def polaczNajmniejszy(self):
        for i in self.slownikTrybuB:
            if i != len(self.slownikTrybuB):
                self.slownikTrybuB[i].dodajPolaczenie(i + 1)
            if i != 1:
                self.slownikTrybuB[i].dodajPolaczenie(i - 1)
            # print(i, '', self.slownikTrybuB[i].polaczenia)

    def polaczLosowo(self, iloscWierzcholkow, iloscNowychPolaczen):
        for i in range(1, iloscNowychPolaczen):
            pulaPunktow = []
            for j in range(1, len(self.slownikTrybuB) + 1):
                pulaPunktow.append(j)
            wierzcholek1 = choice(pulaPunktow)
            while len(self.slownikTrybuB[wierzcholek1].polaczenia) == iloscWierzcholkow-1:
                pulaPunktow.remove(wierzcholek1)
                wierzcholek1 = choice(pulaPunktow)

            wierzcholek2 = choice(pulaPunktow)
            while wierzcholek2 in self.slownikTrybuB[wierzcholek1].polaczenia or wierzcholek2 == wierzcholek1:
                pulaPunktow.remove(wierzcholek2)
                wierzcholek2 = choice(pulaPunktow)


            self.slownikTrybuB[wierzcholek1].dodajPolaczenie(wierzcholek2)
            self.slownikTrybuB[wierzcholek2].dodajPolaczenie(wierzcholek1)
            self.slownikTrybuB[wierzcholek1].polaczenia.sort()
            self.slownikTrybuB[wierzcholek2].polaczenia.sort()
        if self.sprawdzDwudzielnosc(iloscWierzcholkow) == 'tak':
            return 'tak'
        else:
            return 'nie'

    def sprawdzDwudzielnosc(self, iloscWierzcholkow):
        self.aktualnyKrok = 0
        self.odwiedzone = []
        self.kolejka = [1]
        self.kolor1 = 'zielony'
        self.kolor2 = 'czerwony'
        self.bladDwudzielnosci = '<--- brak dwudzielnosci'
        self.slownikTrybuB[1].kolor = self.kolor1

        for i in range(iloscWierzcholkow):
            czyBlad = self.bFS_B()

        if czyBlad == 'blad':
            return 'nie'
        elif czyBlad == 'dwudzielnosc':
            return 'tak'
        else:
            print('brak informacji o dwudzielnosci')

    def bFS_B(self):
        self.odwiedzone.append(self.kolejka[0])
        # w kolejce
        self.kolejka.remove(self.kolejka[0])
        aktualnyWierzcholek = self.slownikTrybuB[self.odwiedzone[-1]]
        for punkt in range(len(aktualnyWierzcholek.polaczenia)):
            if aktualnyWierzcholek.polaczenia[punkt] not in self.kolejka and aktualnyWierzcholek.polaczenia[
                punkt] not in self.odwiedzone:
                self.kolejka.append(aktualnyWierzcholek.polaczenia[punkt])
        # kolor
        for item in aktualnyWierzcholek.polaczenia:
            if self.slownikTrybuB[item].kolor == '':
                if aktualnyWierzcholek.kolor == self.kolor1:
                    self.slownikTrybuB[item].kolor = self.kolor2
                elif aktualnyWierzcholek.kolor == self.kolor2:
                    self.slownikTrybuB[item].kolor = self.kolor1
            elif self.slownikTrybuB[item].kolor != '':
                if self.slownikTrybuB[item].kolor == aktualnyWierzcholek.kolor:
                    #self.slownikTrybuB[item].znacznikBleduDwudzielnosci = self.bladDwudzielnosci
                    return 'blad'

        return 'dwudzielnosc'

    def generujWynikTrybuB(self):
        wyniki = {}
        for i in range(len(self.listaIlosciPolaczen)):
            if self.listaIlosciPolaczen[i] not in wyniki:
                wyniki[self.listaIlosciPolaczen[i]] = 0
            if self.listaIlosciDwudzielnosci[i] == 'tak':
                wyniki[self.listaIlosciPolaczen[i]]+=1
        print('\n\t\twyniki', '\n ------------------------')
        print('ilosc grafow: ', len(self.listaIlosciPolaczen),'\nilosc polaczen\t| % grafow dwudzielnych')
        for key, value in wyniki.items():
            print('\t\t', key, '\t| ', value, '%')
        plt.bar(range(len(wyniki)), list(wyniki.values()), align='center')
        plt.xticks(range(len(wyniki)), list(wyniki.keys()))
        plt.show()


class Wierzcholek:
    def __init__(self):
        self.polaczenia = []
        self.kolor = ''
        self.znacznikBleduDwudzielnosci = ''

    def dodajPolaczenie(self, polaczonyWierzcholek):
        self.polaczenia.append(polaczonyWierzcholek)

print('podaj nazwe pliku dla BFS (domyslnie "graf.txt")')
plik = str(input())
if plik == '':
    Grafomat().trybA('BFS')
else:
    Grafomat(plik).trybA('BFS')
print('podaj nazwe pliku dla DFS (domyslnie "graf.txt")')
plik = str(input())
if plik == '':
    Grafomat().trybA('DFS')
else:
    Grafomat(plik).trybA('DFS')
print('czy rozpoczac Tryb B? (domyslnie "tak")')
accept = str(input())
if accept == '' or 'tak':
    print('trwa generowanie grafów')
    Grafomat().trybB()
